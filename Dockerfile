FROM ubuntu:bionic

MAINTAINER Wimal Perera <Wimal.Perera@objectconsulting.com.au>

######################################
# Environment Variables - Start

# Note: Environment Variables can be overriden via ECS Container Settings within ECS Task Definition.
######################################

# AWS S3 Settings
ENV BUILD_S3_BUCKET "#build_s3_bucket#"
ENV BUILD_S3_PATH "#build_s3_path#"
ENV BUILD_S3_ARTIFACT "#build_s3_artifact#"

# Enable java remote debug or not
ENV ENABLE_REMOTE_DEBUG "false"

# AWS RDS Database Settings to be injected
ENV DB_HOST "db_host"
ENV DB_PORT "db_port"
ENV DB_DATABASE "db_database"
ENV DB_USERNAME "db_username"
ENV DB_PASSWORD "db_password"

# Tomcat memory limits to be adjusted from ansible
ENV JVM_XMS "4096m"
ENV JVM_XX_MAX_PERM_SIZE "2048m"
ENV JVM_XMX "6144m"

#########################################
# Environment Variables - End
#########################################

RUN apt-get update && apt-get upgrade -q -y && \
  apt-get install -y wget curl software-properties-common net-tools logrotate vim

#install-java-with-apt-get-on-ubuntu-18-04 
RUN apt-get update && \
	apt-get install -y openjdk-8-jdk && \
	apt-get install -y ant && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /var/cache/oracle-jdk8-installer;
# Fix certificate issues, found as of # https://bugs.launchpad.net/ubuntu/+source/ca-certificates-java/+bug/983302 
RUN apt-get update && \
	apt-get install -y ca-certificates-java && \
	apt-get clean && \
	update-ca-certificates -f && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /var/cache/oracle-jdk8-installer;
# Setup JAVA_HOME, this is useful for docker commandline 
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/ 
RUN export JAVA_HOME

# Intall tomcat
RUN wget http://www.strategylions.com.au/mirror/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz && \
  mkdir /opt/tomcat && \
  tar xzvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1
ENV CATALINA_HOME /opt/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH

#install AWS cli
RUN apt-get update -q -y && apt-get install -y tzdata && \
 apt-get install -y awscli python3-pip && \
 pip3 install --upgrade awscli
 
#add jmx jar for tomcat8
ADD catalina-jmx-remote.jar /opt/tomcat/lib/catalina-jmx-remote.jar

#add glowroot jars for tomcat8
ADD glowroot-0.13.0-dist /opt/tomcat/glowroot-0.13.0-dist

#add config files
ADD tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml
ADD context.xml /opt/tomcat/webapps/manager/META-INF/context.xml
ADD context.xml /opt/tomcat/webapps/host-manager/META-INF/context.xml

RUN mkdir -p /datadrive/logs/tomcat

ADD setenv.sh /opt/tomcat/bin/setenv.sh
ADD logging.properties /opt/tomcat/conf/logging.properties
ADD server.xml /opt/tomcat/conf/server.xml
ADD logrotate-tomcat.conf /opt/tomcat/conf/logrotate-tomcat.conf

RUN apt-get -y install cron

# ADD .keystore /opt/tomcat/.keystore

# Define working directory.
WORKDIR $CATALINA_HOME

ADD bootstrapper.sh $CATALINA_HOME/bin/bootstrapper.sh

RUN chmod 777 $CATALINA_HOME/bin/bootstrapper.sh && \
 chmod 777 $CATALINA_HOME/bin/setenv.sh 

CMD ["bash", "bootstrapper.sh"]

