# Overriding Memory Settings
export CATALINA_OPTS="$CATALINA_OPTS -Xms$JVM_XMS -XX:MaxPermSize=$JVM_XX_MAX_PERM_SIZE"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx$JVM_XMX"

# JMX Settings to Enable Profiling
export CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.rmi.port=9090 -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port=9090"
export CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
export CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.local.only=false -Djava.rmi.server.hostname=0.0.0.0 -Dcom.sun.management.jmxremote.host=0.0.0.0"

# Adding Glowroot
export CATALINA_OPTS="$CATALINA_OPTS -javaagent:/opt/tomcat/glowroot-0.13.0-dist/glowroot.jar"

# Remote debugging settings
export JPDA_ADDRESS=8000
export JPDA_TRANSPORT=dt_socket

# Catalina Out Settings
export CATALINA_OUT="/datadrive/logs/tomcat/catalina.out"
