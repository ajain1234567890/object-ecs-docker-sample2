#!/bin/bash

(crontab -l ; echo "14 * * * * /usr/sbin/logrotate /opt/tomcat/conf/logrotate-tomcat.conf -v --state /opt/tomcat/conf/logrotate-state &>> /var/log/cron.log" | crontab; cron)

aws s3 cp s3://${BUILD_S3_BUCKET}/${BUILD_S3_PATH}/${BUILD_S3_ARTIFACT} .

mv ${BUILD_S3_ARTIFACT} object-devops.war

jar xvf object-devops.war WEB-INF/classes/application.properties

sed -i 's/^database.hostname=/#&/' WEB-INF/classes/application.properties
sed -i 's/^database.dbname=/#&/' WEB-INF/classes/application.properties
sed -i 's/^database.port=/#&/' WEB-INF/classes/application.properties
sed -i 's/^database.username=/#&/' WEB-INF/classes/application.properties
sed -i 's/^database.password=/#&/' WEB-INF/classes/application.properties

echo 'database.hostname='$DB_HOST >> WEB-INF/classes/application.properties
echo 'database.port='$DB_PORT >> WEB-INF/classes/application.properties
echo 'database.dbname='$DB_DATABASE >> WEB-INF/classes/application.properties
echo 'database.username='$DB_USERNAME >> WEB-INF/classes/application.properties
echo 'database.password='$DB_PASSWORD >> WEB-INF/classes/application.properties

jar uvf object-devops.war WEB-INF/classes/application.properties

mv object-devops.war webapps/object-devops.war

if [ "$ENABLE_REMOTE_DEBUG" == "true" ]; then
    catalina.sh jpda run
else
    catalina.sh run
fi
